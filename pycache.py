#-*- coding: utf-8 -*-
import os

def pycache_test():
    sim = CacheManager()
    sim.setParameters(bits_of_address=32,bytes_of_trace_data=64)
    #sim.OneLevelInit(cache, 24, 4, data_bytes=64)
    sim.TwoLevelInit(cache, 24, 4, 36, 4, data_bytes=64)
    for filename in ['trace/bitcount_V2.txt','trace/crc_V2.txt','trace/patricia_V2.txt','trace/qsort_V2.txt','trace/susan_V2.txt']:
        sim.Trace(filename)
        #break

TRACE_LOG_TIMES = 1
"""***STT-RAM***"""
# write energy
WE_SLC = 3.0
ZT = 0
ST = 1.92
HT = 3.192
TT = ST+HT
# write latency
WL_SLC = 23
WL_ZT = 0
WL_ST = 23
WL_HT = 23
WL_TT = WL_ST+WL_HT
# read energy
RE_SLC = 1.0
RE_MLC = 0.6
RE_ENCODE = 0.9
# read latency
RL_SLC = 7
RL_MLC = 18
RL_ENCODE = 20

def getTransType(str_last, str_next, simple=False):
    """get the transition type list of the two string.
    str_last:(str)original data , must be even length.
    str_next:(str)target data , the length must be same as len(str_last).
    rtn:(list)the list contain var ZT,ST,HT,TT.
    """
    str_len = len(str_last)
    type_list = list()
    if(not(str_len==len(str_next)) or str_len%2):
        return
    for i in range(0,str_len,2):
        if(not simple):     # complex mode
            if(str_last[i]==str_next[i]):
                if(str_last[i+1]==str_next[i+1]):
                    type_list.append(ZT)
                else:
                    type_list.append(ST)
            else:
                if(str_next[i]==str_next[i+1]):
                    type_list.append(HT)
                else:
                    type_list.append(TT)
        else:               # simple mode (CMLC-S) 2021/05/16
            if(str_next[i]==str_next[i+1]):
                type_list.append(HT)
            else:
                type_list.append(TT)
    return type_list
    
"""***File function***"""
def Hex2Bin(string, bits=0):
    """ get the bin string by hex string.
    hihi
    """
    if(bits):
        return bin(int(string,16))[2:].zfill(bits)
    return bin(int(string,16))[2:]

def Bin2Dec(string):
    """ get the Dec int by bin string.
    """
    return int(string,2)

def Dec2Bin(var, bits=0):
    """ get the bin string by Dec int.
    """
    if(bits):
        return bin(var)[2:].zfill(bits)
    return bin(var)[2:]
    
"""***Cache class***"""
class cacheline():
    def __init__(self, tag, data, data_bytes, dirty=False):
        self.tag = tag
        self.data = data
        self.dirty = dirty
        self.valid = True
        
class SRAMcache(object):    # use to be L1
    name = 'SRAM cache'
    def __init__(self, cache_size, way, data_bytes, eviction_mode='LRU'):
        from math import log
        self.cache_size = cache_size    # (KB)
        self.way = way                  # line per set
        self.data_bytes = data_bytes    # bytes per line
        self.eviction_mode = eviction_mode
        self.num_of_set = (self.cache_size*1024)/(self.way*self.data_bytes)
        self.bits_of_index = int(log(self.num_of_set,2))
        if(2**self.bits_of_index<self.num_of_set):  # if the cache size is not divisible , it will abort some space.
            self.num_of_set = 2**self.bits_of_index
            self.cache_size = self.num_of_set*self.way*self.data_bytes/1024
        self.cacheset = [list() for i in range(self.num_of_set)]
    
        self._InstructionCounter = 0
        self._ReadCounter = 0       # read the data of a cacheline (don't care the data size)
        self._UpdateCounter = 0     # update the data of a cacheline (don't care the data size)
        self._Fun_updateCounter = 0
        self._Fun_replacementCounter = 0
        self._Fun_newlineCounter = 0
        self._Fun_send2hostCounter = 0
        self._Fun_write2memCounter = 0
    """Init Functions"""
    def setParameters(self, bits_of_address=32, bits_of_offset=6):
        self.__bits_of_address = bits_of_address
        self.__bits_of_offset = bits_of_offset
    def reset(self):
        self.cacheset = [list() for i in range(self.num_of_set)]
        self._InstructionCounter = 0
        self._ReadCounter = 0
        self._UpdateCounter = 0
        self._Fun_updateCounter = 0
        self._Fun_replacementCounter = 0
        self._Fun_newlineCounter = 0
        self._Fun_send2hostCounter = 0
        self._Fun_write2memCounter = 0
    def get_status(self):
        tmp2 = self.__bits_of_address - self.__bits_of_offset
        tmp1 = tmp2 - self.bits_of_index
        tmplog =  'Scheme:'+self.name+'\n'
        tmplog += 'cache status:'
        tmplog += str(self.cache_size)+'K, ' if self.cache_size<1024 else str(self.cache_size/1024)+'M, '
        tmplog += str(self.way)+'-way, '+self.eviction_mode
        return (tmplog+
                '\naddress bits: %d bits(%d+%d+%d)'%(self.__bits_of_address,tmp1,tmp2-tmp1,self.__bits_of_address-tmp2)+
                '\ntotal sets: %d(2^%d)'%(self.num_of_set,tmp2-tmp1)+
                '\ndata size: %d bits'%(self.data_bytes*8))
    
    """Normal Functions"""
    def execute_cmd(self, cmd):
        self._InstructionCounter += 1
    def is_full(self, set_index):
        return len(self.cacheset[set_index]) == self.way
    def get_hit_index(self, set_index, tag):
        """ get the hit index of cacheset , or None(Miss).
        """
        hit_index = None
        for line in self.cacheset[set_index]:
            if (tag == line.tag and line.valid):
                hit_index = self.cacheset[set_index].index(line)
                break
        return hit_index
    def split_address(self, address):
        """ split the address to (tag(str),index(int),offset(int))
        """
        tmp2 = self.__bits_of_address - self.__bits_of_offset
        tmp1 = tmp2 - self.bits_of_index
        tag = address[:tmp1]
        index = Bin2Dec(address[tmp1:tmp2])
        offset = Bin2Dec(address[tmp2:self.__bits_of_address])
        return tag,index,offset
    def combine_address(self, tag, index, offset):
        """ combine (tag(str),index(int),offset(int)) to address
        """
        return tag+Dec2Bin(index,bits=self.bits_of_index)+Dec2Bin(offset,bits=self.__bits_of_offset)
    def Write(self, cline, tag, data, dirty, valid):
        self._UpdateCounter += 1
        cline.tag = tag
        cline.data = data
        cline.dirty = dirty
        cline.valid = valid
    def Read(self, index, hit_index):
        self._ReadCounter += 1
        return self.cacheset[index][hit_index].data
        
    """Cache Operating Functions(need to implement)"""
    def send2host(self, data):
        pass
    def write2mem(self, data):
        pass
    def get_result(self):                                   # rtn:(str)
        size = ' '
        size += str(self.cache_size)+'K' if self.cache_size<1024 else str(self.cache_size/1024)+'M'
        return '',{'name':self.name.replace('cache','')+size}
    def read(self, index, hit_index):                       # rtn:data
        """ Read hit fuction: Read the cache data by index & hit_index. And LRU update.
        index = index of set
        hit_index = index of cacheline
        """
        data = self.Read(index, hit_index)
        '''LRU mode'''
        if(self.eviction_mode=='LRU'):
            self.cacheset[index].insert(0,self.cacheset[index].pop(hit_index))
        elif(self.eviction_mode=='FIFO'):
            pass
        return data
    def update(self, index, tag, data, hit_index):
        """ Write hit fuction: update the cacheline's tag and data. And LRU update(options).
        """
        '''Write Back mode'''
        self.Write(self.cacheset[index][hit_index], tag, data, True, True)
        '''LRU mode'''
        if(self.eviction_mode=='LRU'):
            self.cacheset[index].insert(0,self.cacheset[index].pop(hit_index))
        elif(self.eviction_mode=='FIFO'):
            pass
        self._Fun_updateCounter += 1
        return
    def replacement(self, index, tag, data, dirty=True):    # rtn:popdict
        """ Miss function: return the popdict(last of the set), then update the cacheline and LRU update.
        """
        popline = self.cacheset[index].pop()
        popdict = {'tag':popline.tag, 'data':popline.data, 'dirty':popline.dirty, 'valid':popline.valid, 'index':index}
        self.Write(popline, tag, data, dirty, True)
        '''LRU mode'''
        if(self.eviction_mode=='LRU' or self.eviction_mode=='FIFO'):
            self.cacheset[index].insert(0,popline)
        self._Fun_replacementCounter += 1
        return popdict
    def newline(self, index, tag, data, dirty=True):
        """ create a new cacheline and add to cacheset. (only use when Miss)
        """
        original_data = str().zfill(len(data)).replace('0','1')
        cline = cacheline(tag, original_data, self.data_bytes, dirty=dirty)
        self.Write(cline, tag, data, dirty, True)
        self.cacheset[index].insert(0, cline)
        self._Fun_newlineCounter += 1
        
class SLCcache(SRAMcache):
    name = 'SLC cache'
    def __init__(self, cache_size, way, data_bytes, eviction_mode='LRU'):
        super(SLCcache, self).__init__(cache_size, way, data_bytes, eviction_mode=eviction_mode)      
    """Get Functions"""
    def get_segment_state(self):
        return False
    def get_result(self):
        size = str(self.cache_size)+'K' if self.cache_size<1024 else str(self.cache_size/1024)+'M'
        cells_per_line = self.data_bytes*8
        bits_per_line = self.data_bytes*8
        r_latency = RL_SLC * self._ReadCounter
        w_latency = WL_SLC * self._UpdateCounter
        
        r_energy = RE_SLC * self._ReadCounter * bits_per_line
        w_energy = WE_SLC * self._UpdateCounter * cells_per_line
        wtime = self._UpdateCounter
        
        latency = r_latency+w_latency
        energy = r_energy+w_energy
        print '----debug log----'
        print 'Write Latency:',w_latency
        print 'Read Latency:',r_latency
        print 'Write Energy:',w_energy
        print 'Read Energy:',r_energy
        print '----debug log----'
        r_dict = {'name':self.name.replace('cache','')+size, 'latency':latency, 'energy':energy, 'wtime':[wtime,0]}
        return ('\n[Status of cells]'+
                '\nTotal write times of cacheline:'+str(self._UpdateCounter)+
                '\nTotal Latency:'+str(latency)+' (%d+%d)'%(r_latency,w_latency)+
                '\nTotal Energy:'+str(energy)+' (%d+%d)'%(r_energy,w_energy)+
                '\n[Func] update called:'+str(self._Fun_updateCounter)+
                '\n[Func] replacement called:'+str(self._Fun_replacementCounter)+
                '\n[Func] newline called:'+str(self._Fun_newlineCounter)+
                '\n[Func] send2host called:'+str(self._Fun_send2hostCounter)+
                '\n[Func] write2mem called:'+str(self._Fun_write2memCounter)),r_dict



class MLCcache(SLCcache):
    CMLCC = False    # True for CMLC-C , False for CMLC-S
    name = 'CMLC-C cache' if CMLCC else 'CMLC-S cache'
    def __init__(self, cache_size, way, data_bytes, eviction_mode='LRU'):
        super(MLCcache, self).__init__(cache_size, way, data_bytes, eviction_mode=eviction_mode)
        self._TTCounter = 0
        self._HTCounter = 0
        self._STCounter = 0
        self._ZTCounter = 0
        self._UpdateCounter_TT = 0
        self._UpdateCounter_noneTT = 0
        self._ReadCounterByWrite = 0        # Add in 2021/05/17
    """Init Functions"""
    def reset(self):
        super(MLCcache, self).reset()
        self._TTCounter = 0
        self._HTCounter = 0
        self._STCounter = 0
        self._ZTCounter = 0
        self._UpdateCounter_TT = 0
        self._UpdateCounter_noneTT = 0
        self._ReadCounterByWrite = 0        # Add in 2021/05/17
        print 'reset'
    
    """Normal Functions"""
    def update_counter(self, trans_type):
        self._TTCounter += trans_type.count(TT)
        self._HTCounter += trans_type.count(HT)
        self._STCounter += trans_type.count(ST)
        self._ZTCounter += trans_type.count(ZT)
        if(TT in trans_type):
            self._UpdateCounter_TT += 1
        else:
            self._UpdateCounter_noneTT += 1
    def get_segment_state(self):
        return [self._TTCounter]
    def get_result(self):
        size = str(self.cache_size)+'K' if self.cache_size<1024 else str(self.cache_size/1024)+'M'
        cells_per_line = self.data_bytes*8/2
        bits_per_line = self.data_bytes*8
        r_latency = RL_MLC * self._ReadCounter
        # w_latency = WL_TT * self._UpdateCounter_TT + ((WL_ST+WL_HT)/2 * self._UpdateCounter_noneTT)
        w_latency = WL_TT * self._UpdateCounter_TT + ((WL_ST+WL_HT)/2 * self._UpdateCounter_noneTT) + RL_MLC * self._ReadCounterByWrite     # 210517
        
        r_energy = RE_MLC * self._ReadCounter * bits_per_line
        # w_energy = ZT*self._ZTCounter + ST*self._STCounter + HT*self._HTCounter + TT*self._TTCounter
        w_energy = ZT*self._ZTCounter + ST*self._STCounter + HT*self._HTCounter + TT*self._TTCounter + RE_MLC * self._ReadCounterByWrite * bits_per_line        # 210517
        
        wtime_s = (self._STCounter+self._HTCounter+(self._TTCounter*2)) / cells_per_line
        wtime_h = (self._HTCounter+self._TTCounter) / cells_per_line
        
        latency = r_latency+w_latency
        energy = r_energy+w_energy
        print '----debug log----'
        print 'Write Latency:',w_latency
        print 'Read Latency:',r_latency
        print 'Write Energy:',w_energy
        print 'Read Energy:',r_energy
        print '----debug log----'
        r_dict = {'name':self.name.replace('cache','')+size, 'latency':[r_latency,w_latency], 'energy':[r_energy,w_energy], 'wtime':[wtime_s,wtime_h], 'stateTrans':[self._ZTCounter,self._STCounter,self._HTCounter,self._TTCounter]}
        return ('\n[Transition Count]'+
                ('\nTT count:'+str(self._TTCounter)).ljust(22)+
                'HT count:'+str(self._HTCounter)+
                ('\nST count:'+str(self._STCounter)).ljust(22)+
                'ZT count:'+str(self._ZTCounter)+
                '\n[Status of cells]'+
                '\nTotal write times(soft domain):'+str(wtime_s)+
                '\nTotal write times(hard domain):'+str(wtime_h)+
                '\nTotal Latency:'+str(latency)+' ( %d + %d )'%(r_latency,w_latency)+
                '\nTotal Energy:'+str(energy)+' ( %d + %d )'%(r_energy,w_energy)+
                '\n[Func] update called:'+str(self._Fun_updateCounter)+
                '\n[Func] replacement called:'+str(self._Fun_replacementCounter)+
                '\n[Func] newline called:'+str(self._Fun_newlineCounter)+
                '\n[Func] send2host called:'+str(self._Fun_send2hostCounter)+
                '\n[Func] write2mem called:'+str(self._Fun_write2memCounter)),r_dict

    """Cache Operating Functions"""
    def update(self, index, tag, data, hit_index):
        """ Write hit fuction: update the cacheline's tag and data. And LRU update(options).
        """
        ### Choose CMLC-C or CMLC-S     2021/05/16
        if(self.CMLCC):  ## CMLC-Complex: Read and evaluate the TransType
            original_data = self.Read(index, hit_index)
            self._ReadCounter -= 1
            self._ReadCounterByWrite += 1
            trans = getTransType(original_data, data)
        else:   ## CMLC-Simple: No read, if data=(00 or 11)->HT , (01 or 10)->TT
            trans = getTransType(self.cacheset[index][hit_index].data, data, simple=True)
        self.update_counter(trans)
        '''Write Back mode'''
        self.Write(self.cacheset[index][hit_index], tag, data, True, True)
        '''LRU mode'''
        if(self.eviction_mode=='LRU'):
            self.cacheset[index].insert(0,self.cacheset[index].pop(hit_index))
        elif(self.eviction_mode=='FIFO'):
            pass
        self._Fun_updateCounter += 1
        return
    def replacement(self, index, tag, data, dirty=True):
        """ Miss function: return the popdict(last of the set), then update the cacheline and LRU update.
        """
        popline = self.cacheset[index].pop()
        ### Choose CMLC-C or CMLC-S     2021/05/18
        if(self.CMLCC):  ## CMLC-Complex: Read and evaluate the TransType
            self._ReadCounterByWrite += 1
            trans = getTransType(popline.data, data)
        else:   ## CMLC-Simple: No read, if data=(00 or 11)->HT , (01 or 10)->TT
            trans = getTransType(popline.data, data, simple=True)
        self.update_counter(trans)
        popdict = {'tag':popline.tag, 'data':popline.data, 'dirty':popline.dirty, 'valid':popline.valid, 'index':index}
        self.Write(popline, tag, data, dirty, True)
        '''LRU mode'''
        if(self.eviction_mode=='LRU' or self.eviction_mode=='FIFO'):
            self.cacheset[index].insert(0,popline)
        self._Fun_replacementCounter += 1
        return popdict
    def newline(self, index, tag, data, dirty=True):
        """ create a new cacheline and add to cacheset. (only use when Miss)
        """
        original_data = str().zfill(len(data)).replace('0','1')
        original_data = str().zfill(len(data))
        cline = cacheline(tag, original_data, self.data_bytes, dirty=dirty)
        self.Write(cline, tag, data, dirty, True)
        ### Choose CMLC-C or CMLC-S     2021/05/18
        if(self.CMLCC):  ## CMLC-Complex: Read and evaluate the TransType
            self._ReadCounterByWrite += 1
            trans = getTransType(original_data, data)
        else:   ## CMLC-Simple: No read, if data=(00 or 11)->HT , (01 or 10)->TT
            trans = getTransType(original_data, data, simple=True)
        self.update_counter(trans)
        self.cacheset[index].insert(0, cline)
        self._Fun_newlineCounter += 1
        
class CacheManager(object):
    def __init__(self):
        self.Cache = list()
        self.__level = 0
        self.__policy = 'inclusive'
        self.__start_time = 0
        self.__end_time = 0
        self.setParameters(bits_of_address=20)
        
        self.write_hit_count = [0,0]
        self.write_miss_count = [0,0]
        self.read_hit_count = [0,0]
        self.read_miss_count = [0,0]
        
    """Init Functions"""
    def setParameters(self, bits_of_address=32, bits_of_offset=6, bytes_of_trace_data=64):
        """ Must be call this function before LevelInit.
        """
        self.__bits_of_address = bits_of_address
        self.__bits_of_offset = bits_of_offset
        self.__bytes_of_trace_data = bytes_of_trace_data
    def reset(self):
        """ reset the counter and cacheset of Cache.
        """
        self.write_hit_count = [0,0]
        self.write_miss_count = [0,0]
        self.read_hit_count = [0,0]
        self.read_miss_count = [0,0]
        for c in self.Cache:
            c.reset()
        return
    def OneLevelInit(self, cache, cache_size, cache_way, data_bytes=64, eviction_mode='LRU'):
        self.__level = 1
        self.Cache = [cache(cache_size,cache_way,data_bytes,eviction_mode=eviction_mode)]
        self.Cache[0].setParameters(bits_of_address=self.__bits_of_address,bits_of_offset=self.__bits_of_offset)
        return True
    def TwoLevelInit(self, mycache, L1_cache_size, L1_cache_way, L2_cache_size, L2_cache_way, data_bytes=64, eviction_mode='LRU', policy='inclusive'):
        if(policy=='inclusive' and (L1_cache_size>L2_cache_size or L1_cache_way>L2_cache_way)):
            self.log('[Error] TwoLevelInit Failed.\n',ltype='error')
            if(L1_cache_size>L2_cache_size):
                self.log('[Error] L1 size(%dK) is larger than L2 size(%dK).\n'%(L1_cache_size,L2_cache_size),ltype='error')
            if(L1_cache_way>L2_cache_way):
                self.log('[Error] L1 %d-way is larger than L2 %d-way.\n'%(L1_cache_way,L2_cache_way),ltype='error')
            return False
        self.__level = 2
        self.__policy = policy
        self.Cache = [SRAMcache(L1_cache_size,L1_cache_way,64,eviction_mode=eviction_mode),mycache(L2_cache_size,L2_cache_way,data_bytes,eviction_mode=eviction_mode)]
        self.Cache[0].setParameters(bits_of_address=self.__bits_of_address,bits_of_offset=self.__bits_of_offset)
        self.Cache[1].setParameters(bits_of_address=self.__bits_of_address,bits_of_offset=self.__bits_of_offset)
        return True
    
    """Normal Functions"""
    def read_from_mem(self):
        pass
    def write2mem(self, popdict):
        if(popdict['dirty'] and popdict['valid']):
            self.Cache[-1].write2mem(popdict['data'])
            self.Cache[-1]._Fun_write2memCounter += 1
        pass
    def send2host(self, data):
        self.Cache[0].send2host(data)
        self.Cache[0]._Fun_send2hostCounter += 1
        pass
    def log(self, msg, ltype='normal'):
        print msg
    def split_line(self, line):
        """ split the line into (cmd, address, data)
        """
        tmp = line.split()
        cmd = tmp[0]
        if('0x' in tmp[1]):
            address = Hex2Bin(tmp[1][2:],bits=self.__bits_of_address)
        elif(tmp[1].isdigit()):
            address = Dec2Bin(int(tmp[1]),bits=self.__bits_of_address)
        if(len(tmp[2])==self.__bytes_of_trace_data*8):
            data = tmp[2]
        else:
            data = Hex2Bin(''.join(tmp[2:]),bits=self.__bytes_of_trace_data*8)
        if(cmd in ['Write','W','write','w']):
            cmd = 'w'
        elif(cmd in ['Read','R','read','r']):
            cmd = 'r'
        else:
            self.log('\n\n[Read Error]unknown cmd: '+cmd+'\n',ltype='error')
            raw_input("Press Enter to exit...")
            exit()
        return cmd, address, data
    def getInfoByAddress(self, address):
        """ split the address into 4 list (every cache's info).
        """
        
        tag = list()
        index = list()
        offset = list()
        hit_index = list()
        for c in self.Cache:
            t,i,o = c.split_address(address)
            index.append(i)
            tag.append(t)
            offset.append(o)
            hit_index.append(c.get_hit_index(i,t))
        return index, tag, offset, hit_index
    def print_status(self):
        if(self.__level == 1):
            tmp = self.Cache[0].get_status()
        elif(self.__level == 2):
            tmp = '-L1-\n'
            tmp += self.Cache[0].get_status()
            tmp +='\n-L2-\n'
            tmp += self.Cache[1].get_status()
        self.log(tmp+'\n')
        return

    """Trace Functions"""
    def OneLevelCmd(self, cmd, address, data):
        """ execute the cmd , using 1-Level policy.
        """
        index, tag, offset, hit_index = self.getInfoByAddress(address)
        if(hit_index[0]!=None):
            ''' Hit '''
            if(cmd=='r'):
                self.read_hit_count[0] += 1
                rdata = self.Cache[0].read(index[0], hit_index[0])
                self.send2host(rdata)
            elif(cmd=='w'):
                self.write_hit_count[0] += 1
                self.Cache[0].update(index[0], tag[0], data, hit_index[0])
        else:
            ''' Miss '''
            if(cmd=='r'):
                self.read_miss_count[0] += 1
                self.read_from_mem()
                mdata = data    # suppose the mdata is from memory
                self.send2host(mdata)
                if(self.Cache[0].is_full(index[0])):
                    popdict = self.Cache[0].replacement(index[0], tag[0], mdata, dirty=False)
                    self.write2mem(popdict)
                else:
                    self.Cache[0].newline(index[0], tag[0], mdata, dirty=False)
            elif(cmd=='w'):
                self.write_miss_count[0] += 1
                ''' write miss policy: write allocate '''
                self.read_from_mem()    # suppose the data is 1111...
                # then fetch to cache (suppose original data is 1111... ,too)
                if(self.Cache[0].is_full(index[0])):
                    popdict = self.Cache[0].replacement(index[0], tag[0], data)
                    self.write2mem(popdict)
                else:
                    self.Cache[0].newline(index[0], tag[0], data)
        self.Cache[0].execute_cmd(cmd)
        return
    def L1eviction(self, popdict):
        """ 2-Level fuction: using L1 popdict to update L2 if dirty.
        """
        if(popdict['dirty'] and popdict['valid']):
            address = self.Cache[0].combine_address(popdict['tag'], popdict['index'],0)
            index, tag, offset, hit_index = self.getInfoByAddress(address)
            self.Cache[1].update(index[1], tag[1], popdict['data'], hit_index[1])
        return
    def L2eviction(self, popdict):
        """ 2-Level function: using L2 popdict to send back invalidation to L1, then write to memory.
        """
        address = self.Cache[1].combine_address(popdict['tag'], popdict['index'],0)
        index, tag, offset, hit_index = self.getInfoByAddress(address)
        if(hit_index[0]!=None): # L1 need to be invalidation.
            popline = self.Cache[0].cacheset[index[0]].pop(hit_index[0])
            if(popline.dirty):
                popdict['data'] = popline.data
                popdict['dirty'] = True
            popline.dirty = False
            popline.valid = False
            self.Cache[0].cacheset[index[0]].append(popline)
        self.write2mem(popdict)
        return
    def TwoLevelCmd(self, cmd, address, data):
        """ execute the cmd , using 2-Level policy.
        """
        index, tag, offset, hit_index = self.getInfoByAddress(address)
        ''' L1 Hit : (Read from/Write to) L1 '''
        if(hit_index[0]!=None):
            if(cmd=='r'):   # read from L1.
                self.read_hit_count[0] += 1
                rdata = self.Cache[0].read(index[0], hit_index[0])
                self.send2host(rdata)
            elif(cmd=='w'): # write to L1.
                self.write_hit_count[0] += 1
                self.Cache[0].update(index[0], tag[0], data, hit_index[0])
            return
        ''' L1 Miss , L2 Hit : (Read from/Write to) L2 , and place in L1 '''
        if(hit_index[1]!=None):
            if(cmd=='r'):   # read from L2 and place in L1 (L1 eviction).
                self.read_miss_count[0] += 1
                self.read_hit_count[1] += 1
                rdata = self.Cache[1].read(index[1], hit_index[1])
                self.send2host(rdata)
                if(self.Cache[0].is_full(index[0])):
                    popdict = self.Cache[0].replacement(index[0], tag[0], rdata, dirty=False)
                    self.L1eviction(popdict)
                else:
                    self.Cache[0].newline(index[0], tag[0], rdata, dirty=False)
            elif(cmd=='w'): # write to L2 and place in L1 (L1 eviction).
                self.write_miss_count[0] += 1
                self.write_hit_count[1] += 1
                self.Cache[1].update(index[1], tag[1], data, hit_index[1])
                if(self.Cache[0].is_full(index[0])):
                    popdict = self.Cache[0].replacement(index[0], tag[0], data)
                    self.L1eviction(popdict)
                else:
                    self.Cache[0].newline(index[0], tag[0], data)
            return
        ''' L1 & L2 Miss : Fetch from memory , and place in L1&L2. '''
        if(cmd=='r'):
            self.read_miss_count[0] += 1
            self.read_miss_count[1] += 1
            self.read_from_mem()
            mdata = data    # suppose the mdata is from memory
            self.send2host(mdata)
            if(self.Cache[0].is_full(index[0])):    # L1 eviction : send to L2
                popdict = self.Cache[0].replacement(index[0], tag[0], mdata, dirty=False)
                self.L1eviction(popdict)
            else:
                self.Cache[0].newline(index[0], tag[0], mdata, dirty=False)
            if(self.Cache[1].is_full(index[1])):    # L2 eviction : send a back invalidation to L1 and write to memory.
                popdict = self.Cache[1].replacement(index[1], tag[1], mdata, dirty=False)
                self.L2eviction(popdict)
            else:
                self.Cache[1].newline(index[1], tag[1], mdata, dirty=False)
        elif(cmd=='w'):
            self.write_miss_count[0] += 1
            self.write_miss_count[1] += 1
            ''' write miss policy: write allocate '''
            self.read_from_mem()    # suppose the data is 1111...
            # then fetch to cache (suppose original data is 1111... ,too)
            if(self.Cache[0].is_full(index[0])):    # L1 eviction : send to L2
                popdict = self.Cache[0].replacement(index[0], tag[0], data)
                self.L1eviction(popdict)
            else:
                self.Cache[0].newline(index[0], tag[0], data)
            if(self.Cache[1].is_full(index[1])):    # L2 eviction : send a back invalidation to L1 and write to mem.
                popdict = self.Cache[1].replacement(index[1], tag[1], data, dirty=False)
                self.L2eviction(popdict)
            else:
                self.Cache[1].newline(index[1], tag[1], data, dirty=False)
        self.Cache[0].execute_cmd(cmd)
        self.Cache[1].execute_cmd(cmd)
        return
    def Trace(self, traceFile):
        import json
        from datetime import datetime
        if(not self.__level):
            self.log('[Error] Trace before Level initialize.\n',ltype='error')
            return
        self.log('Trace file: '+str(traceFile.split(os.sep)[-1])+'\n')
        self.__start_time = datetime.now()
        self.print_status()
        fp = open(traceFile)
        line_length = len(fp.readlines())
        fp.close()
        counter = 0
        percents = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
        for run_time in range(TRACE_LOG_TIMES):
            fp = open(traceFile, "r")
            line = fp.readline()
            while line:
                if(line_length*percents[0]*TRACE_LOG_TIMES<=counter):
                    tmp = str(int(percents[0]*100))+'...'
                    if(len(percents)==2):
                        tmp += '\n'
                    self.log(tmp)
                    percents.pop(0)
                cmd, address, data = self.split_line(line)
                if(self.__level == 1):      # 1-Level cache
                    self.OneLevelCmd(cmd, address, data)
                elif(self.__level == 2):    # 2-Level cache
                    self.TwoLevelCmd(cmd, address, data)
                line = fp.readline()
                counter += 1
            fp.close()
        tmp,r_dict = self.print_result()
        self.__end_time = datetime.now()
        trace_time = self.__end_time - self.__start_time
        if('SLC' in self.Cache[-1].name):
            endurance = (((10**15) / r_dict['wtime'][0]) * line_length)
        else:
            endurance = (((10**14) / r_dict['wtime'][0]) * line_length)
        print 'trace time(os time):',str(trace_time)
        print 'endurance:',endurance
        r_dict['endurance'] = endurance
        self.save_result(traceFile.split('/')[-1], tmp)
        self.reset()
        if(not os.path.isdir('RESULT/tmp')):
            os.mkdir('RESULT/tmp')
        f = open('RESULT/tmp/'+str(traceFile.split(os.sep)[-1]),'a+')
        f.write(json.dumps(r_dict)+'\n')
        f.close()
        
        print 'Trace end.'
    
    """Result Functions"""
    def save_result(self, tracename, tmp):
        from pandas import DataFrame
        tracename = str(tracename.split(os.sep)[-1])
        '''result output'''
        result = list()
        c = self.Cache[self.__level-1]
        size = str(c.cache_size)+'KB' if c.cache_size<1024 else str(c.cache_size/1024)+'MB'
        way = str(c.way)+'way'
        level = str(self.__level)+'Level'
        result.append(
            '【'+' '.join((c.name, size, way, level, "L%d"%(self.Cache.index(c)+1)))+'】\n'+tmp)
        if(not os.path.isdir('RESULT')):
            os.mkdir('RESULT')
        f = open('RESULT'+os.sep+'result_'+tracename,'a+')
        result[0] = 'Trace:'+tracename+'\n\n' + result[0]
        for r in result:
            f.write(r)
            f.write('\n\n')
        f.write('-----------------------\n')
        f.close()
        return
    def print_result(self):
        if(self.__level==1):
            totalhit = self.write_hit_count[0] + self.read_hit_count[0]
            tmp = ('-----result-----'+
                    ('\nwrite hit: %d'%self.write_hit_count[0]).ljust(23)+
                    'read hit: %d'%self.read_hit_count[0]+
                    ('\nwrite miss: %d'%self.write_miss_count[0]).ljust(23)+
                    'read miss: %d'%self.read_miss_count[0]+
                    '\nHit Rate: %f'%(totalhit/1.0/(totalhit+self.write_miss_count[0]+self.read_miss_count[0])))
            result,r_dict = self.Cache[0].get_result()
            tmp += result
        elif(self.__level==2):
            tmp = '-----result-----'
            for i in [0,1]:
                totalhit = self.write_hit_count[i] + self.read_hit_count[i]
                tmp +=  ('\n[L%d cache]'%(i+1)+
                        ('\nL%d write hit: %d'%(i+1,self.write_hit_count[i])).ljust(23)+
                        'L%d read hit: %d'%(i+1,self.read_hit_count[i])+
                        ('\nL%d write miss: %d'%(i+1,self.write_miss_count[i])).ljust(23)+
                        'L%d read miss: %d'%(i+1,self.read_miss_count[i])+
                        '\nL%d Hit Rate: %f'%(i+1,totalhit/1.0/(totalhit+self.write_miss_count[i]+self.read_miss_count[i])))
                result,r_dict = self.Cache[i].get_result()
                tmp += result
        tmp += '\n'
        self.log(tmp+'\n',ltype='success')
        return tmp,r_dict

if __name__ == '__main__':
    pycache_test()
