# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'LogPrinter.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_LogPrinter(object):
    def setupUi(self, Dialog_LogPrinter):
        Dialog_LogPrinter.setObjectName(_fromUtf8("Dialog_LogPrinter"))
        Dialog_LogPrinter.setEnabled(True)
        Dialog_LogPrinter.resize(580, 540)
        Dialog_LogPrinter.setMinimumSize(QtCore.QSize(580, 540))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Courier New"))
        font.setPointSize(11)
        Dialog_LogPrinter.setFont(font)
        self.label_tracefile = QtGui.QLabel(Dialog_LogPrinter)
        self.label_tracefile.setGeometry(QtCore.QRect(0, 10, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.label_tracefile.setFont(font)
        self.label_tracefile.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_tracefile.setObjectName(_fromUtf8("label_tracefile"))
        self.lineEdit_tracefile = QtGui.QLineEdit(Dialog_LogPrinter)
        self.lineEdit_tracefile.setEnabled(True)
        self.lineEdit_tracefile.setGeometry(QtCore.QRect(70, 10, 101, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.lineEdit_tracefile.setFont(font)
        self.lineEdit_tracefile.setAcceptDrops(True)
        self.lineEdit_tracefile.setText(_fromUtf8(""))
        self.lineEdit_tracefile.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.lineEdit_tracefile.setReadOnly(True)
        self.lineEdit_tracefile.setObjectName(_fromUtf8("lineEdit_tracefile"))
        self.label_scheme = QtGui.QLabel(Dialog_LogPrinter)
        self.label_scheme.setGeometry(QtCore.QRect(180, 10, 51, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.label_scheme.setFont(font)
        self.label_scheme.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_scheme.setObjectName(_fromUtf8("label_scheme"))
        self.lineEdit_scheme = QtGui.QLineEdit(Dialog_LogPrinter)
        self.lineEdit_scheme.setEnabled(True)
        self.lineEdit_scheme.setGeometry(QtCore.QRect(230, 10, 71, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.lineEdit_scheme.setFont(font)
        self.lineEdit_scheme.setAcceptDrops(True)
        self.lineEdit_scheme.setText(_fromUtf8(""))
        self.lineEdit_scheme.setReadOnly(True)
        self.lineEdit_scheme.setObjectName(_fromUtf8("lineEdit_scheme"))
        self.textBrowser = QtGui.QTextBrowser(Dialog_LogPrinter)
        self.textBrowser.setEnabled(True)
        self.textBrowser.setGeometry(QtCore.QRect(20, 40, 540, 480))
        self.textBrowser.setMinimumSize(QtCore.QSize(0, 0))
        self.textBrowser.setFocusPolicy(QtCore.Qt.NoFocus)
        self.textBrowser.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.textBrowser.setAcceptDrops(False)
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.pushButton_to_excel = QtGui.QPushButton(Dialog_LogPrinter)
        self.pushButton_to_excel.setGeometry(QtCore.QRect(460, 10, 61, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.pushButton_to_excel.setFont(font)
        self.pushButton_to_excel.setObjectName(_fromUtf8("pushButton_to_excel"))
        self.pushButton_clear = QtGui.QPushButton(Dialog_LogPrinter)
        self.pushButton_clear.setGeometry(QtCore.QRect(410, 10, 51, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.pushButton_clear.setFont(font)
        self.pushButton_clear.setObjectName(_fromUtf8("pushButton_clear"))
        self.checkBox_auto_scroll = QtGui.QCheckBox(Dialog_LogPrinter)
        self.checkBox_auto_scroll.setGeometry(QtCore.QRect(320, 10, 81, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.checkBox_auto_scroll.setFont(font)
        self.checkBox_auto_scroll.setChecked(True)
        self.checkBox_auto_scroll.setObjectName(_fromUtf8("checkBox_auto_scroll"))
        self.pushButton_save = QtGui.QPushButton(Dialog_LogPrinter)
        self.pushButton_save.setGeometry(QtCore.QRect(520, 10, 51, 21))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Calibri"))
        font.setPointSize(10)
        self.pushButton_save.setFont(font)
        self.pushButton_save.setObjectName(_fromUtf8("pushButton_save"))

        self.retranslateUi(Dialog_LogPrinter)
        QtCore.QMetaObject.connectSlotsByName(Dialog_LogPrinter)
        Dialog_LogPrinter.setTabOrder(self.lineEdit_tracefile, self.lineEdit_scheme)
        Dialog_LogPrinter.setTabOrder(self.lineEdit_scheme, self.checkBox_auto_scroll)
        Dialog_LogPrinter.setTabOrder(self.checkBox_auto_scroll, self.pushButton_clear)
        Dialog_LogPrinter.setTabOrder(self.pushButton_clear, self.pushButton_to_excel)
        Dialog_LogPrinter.setTabOrder(self.pushButton_to_excel, self.pushButton_save)

    def retranslateUi(self, Dialog_LogPrinter):
        Dialog_LogPrinter.setWindowTitle(_translate("Dialog_LogPrinter", "Log Printer", None))
        self.label_tracefile.setText(_translate("Dialog_LogPrinter", "Trace File:  ", None))
        self.label_scheme.setText(_translate("Dialog_LogPrinter", "Scheme: ", None))
        self.pushButton_to_excel.setText(_translate("Dialog_LogPrinter", "To excel", None))
        self.pushButton_clear.setText(_translate("Dialog_LogPrinter", "Clear", None))
        self.checkBox_auto_scroll.setText(_translate("Dialog_LogPrinter", "Auto Scroll", None))
        self.pushButton_save.setText(_translate("Dialog_LogPrinter", "Save", None))

