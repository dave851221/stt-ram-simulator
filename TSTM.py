#-*- coding: utf-8 -*-
import os
from pycache import *

def TSTMmain():
    print 'Running TSTM.py'
    #os.chdir("C:\Users\\fredtest\Desktop\AES")
    #os.chdir("C:\Users\user\Desktop")
    print 'Current working directory : "%s"\n'%os.getcwd()
    #CreateTableFile('TSTM_decition_table.xlsx',CreateTable(debug=True))
    
    TSTMsimulator = CacheManager()
    TSTMsimulator.setParameters(bits_of_address=20,bytes_of_trace_data=64)
    #TSTMsimulator.OneLevelInit(TSTMcache, 24, 4, data_bytes=96)
    TSTMsimulator.TwoLevelInit(TSTMcache, 24, 4, 36, 4, data_bytes=96)
    for filename in ['trace/bitcount_V2.txt','trace/crc_V2.txt','trace/patricia_V2.txt','trace/qsort_V2.txt','trace/susan_V2.txt']:
        TSTMsimulator.Trace(filename)
        #break
    return TSTMsimulator

"""***TSTM function***"""
def encodeTSTM(str_data):
    """encode the 4-bits string data to 6-bits data list.
    str_data:(str)4-bits data to encode.
    rtn:(list)the list contain all possible encoded data(length:1~9).
    """
    from itertools import product
    encode_table = {'00':['000'],'01':['001','010','100'],
                    '10':['110','101','011'],'11':['111']}
    if(len(str_data)!=4):
        return
    code = list(product(encode_table[str_data[:2]],encode_table[str_data[2:]]))
    for i in range(len(code)):
        code[i] = code[i][0]+code[i][1]
    return code

def CreateTable(debug=False):
    """create the decition table.
    rtn:(list)decition table.
    """
    row = [bin(x)[2:].zfill(6) for x in range(2**6)]
    column = [bin(x)[2:].zfill(4) for x in range(2**4)]
    table = list()
    trans_table = list()
    TTcount = 0
    for str_last in row:
        code_row = list()
        trans_row = list()
        for str_data in column:
            str_next_list = encodeTSTM(str_data)
            min_energy = 10*TT
            min_energy_index = 0
            flag = True    #True:unavoidable TT , False:this encode can without TT
            for index in range(len(str_next_list)):
                type_list = getTransType(str_last,str_next_list[index])
                if(not TT in type_list):
                    if(min_energy > sum(type_list) or flag):
                        min_energy = sum(type_list)
                        min_energy_index = index
                    flag = False
                elif(min_energy > sum(type_list) and flag):
                    min_energy = sum(type_list)
                    min_energy_index = index
            trans_row.append(getTransType(str_last,str_next_list[min_energy_index]))
            # if flag:
                # code_row.append([str_next_list[min_energy_index]])
            # else:
            code_row.append(str_next_list[min_energy_index])
            if(debug and flag):
                sum_list = list()
                TTcount += 1
                for str_next in str_next_list:
                    sum_list.append(sum(getTransType(str_last,str_next)))
                print 'unavoidable TT from [%s] to [%s]. table: [%s] to [%s] '%(str_last,str_data,str_last,str_next_list[min_energy_index]),
                print 'sum:',sum_list,'(%d)'%min_energy_index
        table.append(code_row)
        trans_table.append(trans_row)
    if(debug):
        print 'unavoidable TT count:', TTcount
    return table, trans_table

def CreateTableFile(filename,table):
    """create the decition table file(excel) by filename.
    filename:(str)must be include the filename extension.
    table:(list)must be create by CreateTable or CreateCompleteTable.
    """
    from pandas import DataFrame
    row = [bin(x)[2:].zfill(6) for x in range(2**6)]
    column = [bin(x)[2:].zfill(4) for x in range(2**4)]
    df_table = DataFrame(table,index=row, columns=column)
    df_table.to_excel(filename)

def CreateConventionalTable(createFile=False,filename='CoventionalTable.xlsx'):
    """create the conventional table by filename. (useless)
    """
    row = [bin(x)[2:].zfill(4) for x in range(2**4)]
    column = [bin(x)[2:].zfill(4) for x in range(2**4)]
    table = list()
    counter = 0
    for str_last in row:
        code_row = list()
        for str_data in column:
            type_list = getTransType(str_last,str_data)
            type_list_str = str()
            for t in type_list:
                if(t==ZT):
                    type_list_str += 'ZT,'
                elif(t==ST):
                    type_list_str += 'ST,'
                elif(t==HT):
                    type_list_str += 'HT,'
                elif(t==TT):
                    type_list_str += 'TT,'
            type_list_str = type_list_str[:-1]
            if('TT' in type_list_str):
                type_list_str = '(' + type_list_str + ')'
                counter += 1
            code_row.append(type_list_str)
        table.append(code_row)
    print 'TT/total=',counter,'/256'
    if(createFile):
        print 'creating...'
        from pandas import DataFrame
        df_table = DataFrame(table,index=row, columns=column)
        df_table.to_excel(filename)
    else:
        print table

"""***TSTM class***"""
class TSTM_Encoder():
    def __init__(self):
        self.__row = [bin(x)[2:].zfill(6) for x in range(2**6)]
        self.__column = [bin(x)[2:].zfill(4) for x in range(2**4)]
        self.__table, self.__trans_table = CreateTable()
    def encode(self, str_last, str_data):
        ''' encode the str_data with str_last
        str_last:(str)6 bits data
        str_data:(str)4 bits data
        rtn:(str,bool)6 bits encoded_code and is unavoidable TT or not
        '''
        if(not len(str_data)==4):
            return
        row_index = self.__row.index(str_last)
        column_index = self.__column.index(str_data)
        encoded_code = self.__table[row_index][column_index]
        trans_type = self.__trans_table[row_index][column_index]
        return encoded_code, trans_type

class TSTM_Decoder():
    def __init__(self):
        self.code_table = {'00':['000'],
                           '01':['001','010','100'],
                           '10':['110','101','011'],
                           '11':['111']}
        return
    def decode(self,str_code):
        str_data = str()
        for i in range(0,len(str_code),3):
            code = str()
            for c in [bin(x)[2:].zfill(2) for x in range(2**2)]:
                if(str_code[i:i+3] in self.code_table[c]):
                    code = c
                    break
            str_data += code
        return str_data

"""***TSTM cache***"""
class TSTMcache(MLCcache):
    name = 'TSTM cache'
    def __init__(self, cache_size, way, data_bytes, eviction_mode='LRU'):
        super(TSTMcache, self).__init__(cache_size, way, data_bytes, eviction_mode=eviction_mode)
        self.__encoder = TSTM_Encoder()
        self.__decoder = TSTM_Decoder()
        self.TT_record = [0,0,0]
    def reset(self):
        super(TSTMcache, self).reset()
    
    """Normal Functions"""
    def send2host(self, data):
        if(len(data)==self.data_bytes*8):
            self.decode(data)
        pass
    def write2mem(self, data):
        if(len(data)==self.data_bytes*8):
            self.decode(data)
        pass
    def get_segment_state(self):
        return self.TT_record
    def get_result(self):
        size = str(self.cache_size)+'K' if self.cache_size<1024 else str(self.cache_size/1024)+'M'
        cells_per_line = self.data_bytes*8/2
        bits_per_line = self.data_bytes*8
        r_latency = RL_ENCODE * self._ReadCounter
        # w_latency = WL_TT * self._UpdateCounter_TT + ((WL_ST+WL_HT)/2 * self._UpdateCounter_noneTT)
        w_latency = WL_TT * self._UpdateCounter_TT + ((WL_ST+WL_HT)/2 * self._UpdateCounter_noneTT) + RL_ENCODE * self._ReadCounterByWrite      # 210517
        
        r_energy = RE_ENCODE * self._ReadCounter * bits_per_line
        # w_energy = ZT*self._ZTCounter + ST*self._STCounter + HT*self._HTCounter + TT*self._TTCounter
        w_energy = ZT*self._ZTCounter + ST*self._STCounter + HT*self._HTCounter + TT*self._TTCounter + RE_ENCODE * self._ReadCounterByWrite * bits_per_line     # 210517
        
        wtime_s = (self._STCounter+self._HTCounter+(self._TTCounter*2)) / cells_per_line
        wtime_h = (self._HTCounter+self._TTCounter) / cells_per_line
        
        latency = r_latency+w_latency
        energy = r_energy+w_energy
        print '----debug log----'
        print 'Write Latency:',w_latency
        print 'Read Latency:',r_latency
        print 'Write Energy:',w_energy
        print 'Read Energy:',r_energy
        print '----debug log----'
        r_dict = {'name':self.name.replace('cache','')+size, 'latency':[r_latency,w_latency], 'energy':[r_energy,w_energy], 'wtime':[wtime_s,wtime_h], 'totalTT':self.TT_record, 'stateTrans':[self._ZTCounter,self._STCounter,self._HTCounter,self._TTCounter]}
        return ('\n[Transition Count]'+
                ('\nTT count:'+str(self._TTCounter)).ljust(22)+
                'HT count:'+str(self._HTCounter)+
                ('\nST count:'+str(self._STCounter)).ljust(22)+
                'ZT count:'+str(self._ZTCounter)+
                '\n[Status of cells]'+
                '\nTotal write times(soft domain):'+str(wtime_s)+
                '\nTotal write times(hard domain):'+str(wtime_h)+
                '\nTotal Latency:'+str(latency)+' ( %d + %d )'%(r_latency,w_latency)+
                '\nTotal Energy:'+str(energy)+' ( %d + %d )'%(r_energy,w_energy)+
                '\nSegment TT counter:'+str(self.TT_record)+
                '\n[Func] update called:'+str(self._Fun_updateCounter)+
                '\n[Func] replacement called:'+str(self._Fun_replacementCounter)+
                '\n[Func] newline called:'+str(self._Fun_newlineCounter)+
                '\n[Func] send2host called:'+str(self._Fun_send2hostCounter)+
                '\n[Func] write2mem called:'+str(self._Fun_write2memCounter)),r_dict
        pass
        
    """Coding Functions"""
    def decode(self, data_encoded):
        return self.__decoder.decode(data_encoded)
    def encode(self, original_data, data):
        data_encoded = str()
        for i in range(0,len(original_data),6):
            ori_code = original_data[i:i+6]
            target_code = data[i*2/3:i*2/3+4]
            encoded, trans_type = self.__encoder.encode(ori_code, target_code)
            if(TT in trans_type):
                self.TT_record[trans_type.index(TT)] += 1
            data_encoded += encoded
        trans = getTransType(original_data, data_encoded)
        return data_encoded, trans
    
    """Cache Operating Functions"""
    def update(self, index, tag, data, hit_index):
        """ Write hit fuction: update the cacheline's tag and data. And LRU update(options).
        """
        original_data = self.Read(index, hit_index)
        # Add in 2021/05/17
        self._ReadCounter -= 1
        self._ReadCounterByWrite += 1
        # Add in 2021/05/17
        data_encoded, trans = self.encode(original_data, data)
        self.update_counter(trans)
        '''write back mode: change the data of cache, then set dirty.'''
        self.Write(self.cacheset[index][hit_index], tag, data_encoded, True, True)
        '''LRU mode'''
        if(self.eviction_mode=='LRU'):
            self.cacheset[index].insert(0,self.cacheset[index].pop(hit_index))
        elif(self.eviction_mode=='FIFO'):
            pass
        self._Fun_updateCounter += 1
        return
    def replacement(self, index, tag, data, dirty=True):
        """ Miss function: return the popdict(last of the set), then update the cacheline and LRU update.
        """
        self.Read(index, -1)# to add the read counter
        # Add in 2021/05/17
        self._ReadCounter -= 1
        self._ReadCounterByWrite += 1
        # Add in 2021/05/17
        popline = self.cacheset[index].pop()
        popdict = {'tag':popline.tag, 'data':self.decode(popline.data), 'dirty':popline.dirty, 'valid':popline.valid, 'index':index}
        data_encoded, trans = self.encode(popline.data, data)
        self.update_counter(trans)
        self.Write(popline, tag, data_encoded, dirty, True)
        '''LRU mode'''
        if(self.eviction_mode=='LRU' or self.eviction_mode=='FIFO'):
            self.cacheset[index].insert(0,popline)
        self._Fun_replacementCounter += 1
        return popdict
    def newline(self, index, tag, data, dirty=True):
        """ create a new cacheline and add to cacheset. (only use when Miss)
        """
        original_data = str().zfill(len(data)*3/2).replace('0','1')
        original_data = str().zfill(len(data)*3/2)
        cline = cacheline(tag, original_data, self.data_bytes, dirty=dirty)
        data_encoded, trans = self.encode(original_data, data)
        self.update_counter(trans)
        self.Write(cline, tag, data_encoded, dirty, True)
        self.cacheset[index].insert(0, cline)
        self.Read(index, 0) # to add the read counter
        # Add in 2021/05/17
        self._ReadCounter -= 1
        self._ReadCounterByWrite += 1
        # Add in 2021/05/17
        self._Fun_newlineCounter += 1
        
if __name__ == '__main__':
    TSTMsimulator = TSTMmain()
