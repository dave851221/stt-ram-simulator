import sys,os,threading,json
import pycache,TSTM,AES
from PyQt4 import QtCore,QtGui
from Simulator import *
from LogPrinter import *
from Options import *

threads = list()
configurationFile = 'config.json'
running = False
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

def ChangeGlobalVar(config):
    write_energy = [config['WriteE']['ZT'],config['WriteE']['ST'],config['WriteE']['HT'],config['WriteE']['TT']]
    write_latency = [config['WriteL']['ZT'],config['WriteL']['ST'],config['WriteL']['HT'],config['WriteL']['TT']]
    read_energy = [config['ReadE']['SLC'],config['ReadE']['CMLC'],config['ReadE']['TSTM']]
    read_latency = [config['ReadL']['SLC'],config['ReadL']['CMLC'],config['ReadL']['TSTM']]
    pycache.WE_SLC = config['WriteE']['SLC']
    pycache.ZT = write_energy[0]
    pycache.ST = write_energy[1]
    pycache.HT = write_energy[2]
    pycache.TT = write_energy[3]
    pycache.WL_SLC = config['WriteL']['SLC']
    pycache.WL_ZT = write_latency[0]
    pycache.WL_ST = write_latency[1]
    pycache.WL_HT = write_latency[2]
    pycache.WL_TT = write_latency[3]
    pycache.RE_SLC = read_energy[0]
    pycache.RE_MLC = read_energy[1]
    pycache.RE_ENCODE = read_energy[2]
    pycache.RL_SLC = read_latency[0]
    pycache.RL_MLC = read_latency[1]
    pycache.RL_ENCODE = read_latency[2]
    pycache.TRACE_LOG_TIMES = config['Trace']['times']
    AES.WEARLEVELER_THRESHOLD = config['threshold']
    
def thr_manager():
    from time import sleep
    global threads,running
    running = True
    i = 0
    while(True):
        if(not len(threads)):
            break
        if(type(threads[0])==list):
            ChangeGlobalVar(threads[0][3])
            logprinter.initial_layout(threads[0][1],threads[0][2])
            threads[0][0].start()
            while(threads[0][0].is_alive()):
                if(not logprinter.isVisible()):
                    print 'end of thr_manager'
                    threads = list()
                    running = False
                    return
                sleep(0.5)
            threads.pop(0)
            if(len(threads)):
                logprinter.log('Ready to next tracing.\n\n', ltype='hint')
                sleep(0.1)
            else:
                logprinter.log('Finish.', ltype='hint')
        else:
            threads[0].start()
            while(threads[0].is_alive()):
                if(not logprinter.isVisible()):
                    print 'end of thr_manager'
                    threads = list()
                    running = False
                    return
                sleep(0.5)
            threads.pop(0)
    print 'end of thr_manager'
    threads = list()
    running = False

def read_config():
    if(not os.path.isfile(configurationFile)):
        config = {'SLC':{'enable':False, 'level':2, 'datasize':64, 'policy':0, 'L1':[32,1], 'L2':[32,4]},
                'CMLC':{'enable':False, 'level':2, 'datasize':64, 'policy':0, 'L1':[32,1], 'L2':[64,4]},
                'TSTM':{'enable':False, 'level':2, 'datasize':96, 'policy':0, 'L1':[32,1], 'L2':[96,4]},
                'AES':{'enable':False, 'level':2, 'datasize':96, 'policy':0, 'L1':[32,1], 'L2':[96,4]},
                'Trace':{'address_bits':32, 'data_bytes':64, 'times':1, 'path':'./', 'file':[]},
                'WriteE':{'SLC':3.0, 'ZT':0, 'ST':1.92, 'HT':3.192, 'TT':5.112},
                'ReadE':{'SLC':1.0, 'CMLC':0.6, 'TSTM':0.9},
                'WriteL':{'SLC':23, 'ZT':0, 'ST':23, 'HT':23, 'TT':46},
                'ReadL':{'SLC':7, 'CMLC':18, 'TSTM':20},
                'threshold':0}
        save_config(config)
        return config
    f = open(configurationFile,'r')
    try:
        config = json.loads(f.read())
        for key in ['SLC','CMLC','TSTM','AES','Trace','WriteE','ReadE','WriteL','ReadL','threshold']:
            if(not config.has_key(key)):
                f.close()
                os.remove(configurationFile)
                config = read_config()
                break
    except:
        f.close()
        os.remove(configurationFile)
        config = read_config()
    f.close()
    return config
    
def save_config(config):
    f = open(configurationFile,'w')
    f.write(json.dumps(config))
    f.close()

class MyCacheManager(pycache.CacheManager):
    def __init__(self):
        super(MyCacheManager, self).__init__()
    def log(self, msg, ltype='normal'):
        print msg,
        logprinter.log(msg, ltype=ltype)

class Options_Manager(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.UI = Ui_Dialog_Options()
        self.UI.setupUi(self)
        self.initial_layout()
        # setting the callback function
        self.UI.pushButton_save.clicked.connect(self.onSave)
        self.UI.pushButton_cancel.clicked.connect(self.onCancel)
    def initial_layout(self):
        config = read_config()
        # LineEdit setting:
        self.UI.lineEdit_times.setText(_translate("Dialog_Options", str(config['Trace']['times']), None))
        self.UI.lineEdit_address_length.setText(_translate("Dialog_Options", str(config['Trace']['address_bits']), None))
        self.UI.lineEdit_datasize.setText(_translate("Dialog_Options", str(config['Trace']['data_bytes']), None))
        
        self.UI.lineEdit_SLC_write_latency.setText(_translate("Dialog_Options", str(config['WriteL']['SLC']), None))
        self.UI.lineEdit_ZT_write_latency.setText(_translate("Dialog_Options", str(config['WriteL']['ZT']), None))
        self.UI.lineEdit_ST_write_latency.setText(_translate("Dialog_Options", str(config['WriteL']['ST']), None))
        self.UI.lineEdit_HT_write_latency.setText(_translate("Dialog_Options", str(config['WriteL']['HT']), None))
        self.UI.lineEdit_TT_write_latency.setText(_translate("Dialog_Options", str(config['WriteL']['TT']), None))
        
        self.UI.lineEdit_SLC_write_energy.setText(_translate("Dialog_Options", str(config['WriteE']['SLC']), None))
        self.UI.lineEdit_ZT_write_energy.setText(_translate("Dialog_Options", str(config['WriteE']['ZT']), None))
        self.UI.lineEdit_ST_write_energy.setText(_translate("Dialog_Options", str(config['WriteE']['ST']), None))
        self.UI.lineEdit_HT_write_energy.setText(_translate("Dialog_Options", str(config['WriteE']['HT']), None))
        self.UI.lineEdit_TT_write_energy.setText(_translate("Dialog_Options", str(config['WriteE']['TT']), None))
        
        self.UI.lineEdit_SLC_read_latency.setText(_translate("Dialog_Options", str(config['ReadL']['SLC']), None))
        self.UI.lineEdit_CMLC_read_latency.setText(_translate("Dialog_Options", str(config['ReadL']['CMLC']), None))
        self.UI.lineEdit_TSTM_read_latency.setText(_translate("Dialog_Options", str(config['ReadL']['TSTM']), None))
        
        self.UI.lineEdit_SLC_read_energy.setText(_translate("Dialog_Options", str(config['ReadE']['SLC']), None))
        self.UI.lineEdit_CMLC_read_energy.setText(_translate("Dialog_Options", str(config['ReadE']['CMLC']), None))
        self.UI.lineEdit_TSTM_read_energy.setText(_translate("Dialog_Options", str(config['ReadE']['TSTM']), None))
    def get_now_config(self):
        config = read_config()
        config['Trace']['times'] = int(self.UI.lineEdit_times.text())
        config['Trace']['address_bits'] = int(self.UI.lineEdit_address_length.text())
        config['Trace']['data_bytes'] = int(self.UI.lineEdit_datasize.text())
        config['WriteL']['SLC'] = float(self.UI.lineEdit_SLC_write_latency.text())
        config['WriteL']['ZT'] = float(self.UI.lineEdit_ZT_write_latency.text())
        config['WriteL']['ST'] = float(self.UI.lineEdit_ST_write_latency.text())
        config['WriteL']['HT'] = float(self.UI.lineEdit_HT_write_latency.text())
        config['WriteL']['TT'] = float(self.UI.lineEdit_TT_write_latency.text())
        config['WriteE']['SLC'] = float(self.UI.lineEdit_SLC_write_energy.text())
        config['WriteE']['ZT'] = float(self.UI.lineEdit_ZT_write_energy.text())
        config['WriteE']['ST'] = float(self.UI.lineEdit_ST_write_energy.text())
        config['WriteE']['HT'] = float(self.UI.lineEdit_HT_write_energy.text())
        config['WriteE']['TT'] = float(self.UI.lineEdit_TT_write_energy.text())
        config['ReadL']['SLC'] = float(self.UI.lineEdit_SLC_read_latency.text())
        config['ReadL']['CMLC'] = float(self.UI.lineEdit_CMLC_read_latency.text())
        config['ReadL']['TSTM'] = float(self.UI.lineEdit_TSTM_read_latency.text())
        config['ReadE']['SLC'] = float(self.UI.lineEdit_SLC_read_energy.text())
        config['ReadE']['CMLC'] = float(self.UI.lineEdit_CMLC_read_energy.text())
        config['ReadE']['TSTM'] = float(self.UI.lineEdit_TSTM_read_energy.text())
        return config
    def onSave(self):
        config = self.get_now_config()
        save_config(config)
        self.close()
    def onCancel(self):
        self.close()
        
class Log_Printer(QtGui.QMainWindow):
    auto_scroll = True
    red = QtGui.QColor(255, 0, 0)
    green = QtGui.QColor(0, 200, 0)
    blue = QtGui.QColor(0, 0, 255)
    black = QtGui.QColor(0, 0, 0)
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.UI = Ui_Dialog_LogPrinter()
        self.UI.setupUi(self)
        # setting the callback function
        self.UI.pushButton_save.clicked.connect(self.onSave)
        self.UI.pushButton_to_excel.clicked.connect(self.save2excel)
        self.UI.pushButton_clear.clicked.connect(lambda:self.UI.textBrowser.clear())
        self.UI.checkBox_auto_scroll.stateChanged.connect(lambda state:self.change_checkBox(state))
    def initial_layout(self, filename, scheme):
        self.UI.lineEdit_tracefile.setText(_translate("Dialog_LogPrinter", filename, None))
        self.UI.lineEdit_scheme.setText(_translate("Dialog_LogPrinter", scheme, None))
    def change_checkBox(self,state):
        self.auto_scroll = bool(state)
    def onSave(self):
        filename = QtGui.QFileDialog.getSaveFileName(self,"LogFile","./",
        "Text Files (*.txt)")
        f = open(filename,'w+')
        f.write(self.UI.textBrowser.toPlainText())
        f.close()
    def save2excel(self):
        from pandas import DataFrame
        from datetime import datetime
        wtimes = {'Trace':[]}
        energy = {'Trace':[]}
        latency = {'Trace':[]}
        totalTT = {'Trace':[]}
        stateTrans = {'Trace':[]}
        endurance = {'Trace':[]}
        for r in os.walk('./RESULT/tmp'):
            if(len(r[1])):
                continue
            for filename in r[2]:
                print 'FILE:',filename
                f = open('./RESULT/tmp/'+filename,'r')
                wtimes['Trace'].append(filename.split('.')[0])
                energy['Trace'].append(filename.split('.')[0])
                latency['Trace'].append(filename.split('.')[0])
                totalTT['Trace'].append(filename.split('.')[0])
                stateTrans['Trace'].append(filename.split('.')[0])
                endurance['Trace'].append(filename.split('.')[0])
                while(True):
                    line = f.readline()
                    if(not line or line==''):
                        break
                    rdict = json.loads(line)
                    if('SRAM' in rdict['name']):
                        continue
                    # write time , energy , latency , stateTrans
                    if('SLC' in rdict['name']):
                        #if(wtimes.has_key(rdict['name'])):
                        if(r[2].index(filename)):
                            wtimes[rdict['name']].append(rdict['wtime'][0])
                            energy[rdict['name']].append(rdict['energy'])
                            latency[rdict['name']].append(rdict['latency'])
                        else:
                            wtimes[rdict['name']] = [rdict['wtime'][0]]
                            energy[rdict['name']] = [rdict['energy']]
                            latency[rdict['name']] = [rdict['latency']]
                        
                    else:
                        #if(wtimes.has_key(rdict['name']+' soft')):
                        if(r[2].index(filename)):
                            wtimes[rdict['name']+' soft'].append(rdict['wtime'][0])
                            wtimes[rdict['name']+' hard'].append(rdict['wtime'][1])
                            energy[rdict['name']+' read'].append(rdict['energy'][0])
                            energy[rdict['name']+' write'].append(rdict['energy'][1])
                            latency[rdict['name']+' read'].append(rdict['latency'][0])
                            latency[rdict['name']+' write'].append(rdict['latency'][1])
                            for i in range(4):
                                stateTrans[rdict['name']+' '+['ZT','ST','HT','TT'][i]].append(rdict['stateTrans'][i])
                        else:
                            wtimes[rdict['name']+' soft'] = [rdict['wtime'][0]]
                            wtimes[rdict['name']+' hard'] = [rdict['wtime'][1]]
                            energy[rdict['name']+' read'] = [rdict['energy'][0]]
                            energy[rdict['name']+' write'] = [rdict['energy'][1]]
                            latency[rdict['name']+' read'] = [rdict['latency'][0]]
                            latency[rdict['name']+' write'] = [rdict['latency'][1]]
                            for i in range(4):
                                stateTrans[rdict['name']+' '+['ZT','ST','HT','TT'][i]] = [rdict['stateTrans'][i]]
                    # endurance
                    if(r[2].index(filename)):
                        endurance[rdict['name']].append(rdict['endurance'])
                    else:
                        endurance[rdict['name']] = [rdict['endurance']]
                        
                    # total TT count
                    if('TSTM' in rdict['name'] or 'AES' in rdict['name']):
                        #if(totalTT.has_key(rdict['name']+' first')):
                        if(r[2].index(filename)):
                            totalTT[rdict['name']+' first'].append(rdict['totalTT'][0])
                            totalTT[rdict['name']+' mid'].append(rdict['totalTT'][1])
                            totalTT[rdict['name']+' last'].append(rdict['totalTT'][2])
                        else:
                            totalTT[rdict['name']+' first'] = [rdict['totalTT'][0]]
                            totalTT[rdict['name']+' mid'] = [rdict['totalTT'][1]]
                            totalTT[rdict['name']+' last'] = [rdict['totalTT'][2]]
                f.close()
        wtimes_df = DataFrame.from_dict(wtimes)
        energy_df = DataFrame.from_dict(energy)
        latency_df = DataFrame.from_dict(latency)
        totalTT_df = DataFrame.from_dict(totalTT)
        stateTrans_df = DataFrame.from_dict(stateTrans)
        endurance_df = DataFrame.from_dict(endurance)
        
        wtimes_df.set_index('Trace',inplace=True)
        energy_df.set_index('Trace',inplace=True)
        latency_df.set_index('Trace',inplace=True)
        totalTT_df.set_index('Trace',inplace=True)
        stateTrans_df.set_index('Trace',inplace=True)
        stateTrans_df = stateTrans_df.T
        endurance_df.set_index('Trace',inplace=True)
        
        wtimes_df.to_excel('RESULT/result-write time '+str(datetime.now())[:19].replace(":",".")+'.xlsx')
        energy_df.to_excel('RESULT/result-energy '+str(datetime.now())[:19].replace(":",".")+'.xlsx')
        latency_df.to_excel('RESULT/result-latency '+str(datetime.now())[:19].replace(":",".")+'.xlsx')
        totalTT_df.to_excel('RESULT/result-total TT '+str(datetime.now())[:19].replace(":",".")+'.xlsx')
        stateTrans_df.to_excel('RESULT/result-state transition '+str(datetime.now())[:19].replace(":",".")+'.xlsx')
        endurance_df.to_excel('RESULT/result-endurance '+str(datetime.now())[:19].replace(":",".")+'.xlsx')
        
        self.log('\n\nSuccess to export the excel file!', ltype='success')
    def log(self, msg, ltype='normal'):
        #self.UI.textBrowser.append(msg)    #auto next line
        if(ltype=='normal'):
            self.UI.textBrowser.setTextColor(self.black)
        elif(ltype=='error'):
            self.UI.textBrowser.setTextColor(self.red)
        elif(ltype=='hint'):
            self.UI.textBrowser.setTextColor(self.blue)
        elif(ltype=='success'):
            self.UI.textBrowser.setTextColor(self.green)
        self.UI.textBrowser.insertPlainText(msg)
        if(self.auto_scroll):
            self.UI.textBrowser.moveCursor(QtGui.QTextCursor.End)

class STT_RAM_Simulator(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.UI = Ui_Dialog_Simulator()
        self.UI.setupUi(self)
        self.initial_layout()
        self.cacheManagers = [MyCacheManager(),MyCacheManager(),MyCacheManager(),MyCacheManager()]
        # setting the callback function
        self.UI.checkBox_SLC.stateChanged.connect(lambda state:self.change_checkBox('SLC',state))
        self.UI.checkBox_CMLC.stateChanged.connect(lambda state:self.change_checkBox('CMLC',state))
        self.UI.checkBox_TSTM.stateChanged.connect(lambda state:self.change_checkBox('TSTM',state))
        self.UI.checkBox_AES.stateChanged.connect(lambda state:self.change_checkBox('AES',state))
        self.UI.comboBox_SLC.currentIndexChanged.connect(lambda index:self.change_comboBox('SLC',index))
        self.UI.comboBox_CMLC.currentIndexChanged.connect(lambda index:self.change_comboBox('CMLC',index))
        self.UI.comboBox_TSTM.currentIndexChanged.connect(lambda index:self.change_comboBox('TSTM',index))
        self.UI.comboBox_AES.currentIndexChanged.connect(lambda index:self.change_comboBox('AES',index))
        self.UI.pushButton_Options.clicked.connect(lambda:self.click_pushButton('Options'))
        self.UI.pushButton_Start.clicked.connect(lambda:self.click_pushButton('Start'))
        self.UI.toolButton.clicked.connect(self.select_trace)
    def initial_layout(self):
        # read the configuration file then set the layout.
        config = read_config()
        
        # CheckBox setting: default => non checked
        self.UI.checkBox_SLC.setChecked(config['SLC']['enable'])
        self.UI.checkBox_CMLC.setChecked(config['CMLC']['enable'])
        self.UI.checkBox_TSTM.setChecked(config['TSTM']['enable'])
        self.UI.checkBox_AES.setChecked(config['AES']['enable'])
        self.UI.groupBox_SLC.setEnabled(config['SLC']['enable'])
        self.UI.groupBox_CMLC.setEnabled(config['CMLC']['enable'])
        self.UI.groupBox_TSTM.setEnabled(config['TSTM']['enable'])
        self.UI.groupBox_AES.setEnabled(config['AES']['enable'])
        
        # ComboBox setting: default => index=1(2-Level)
        self.UI.comboBox_SLC.setCurrentIndex(config['SLC']['level']-1)
        self.UI.comboBox_CMLC.setCurrentIndex(config['CMLC']['level']-1)
        self.UI.comboBox_TSTM.setCurrentIndex(config['TSTM']['level']-1)
        self.UI.comboBox_AES.setCurrentIndex(config['AES']['level']-1)
        self.UI.comboBox_SLC_policy.setCurrentIndex(config['SLC']['policy'])
        self.UI.comboBox_CMLC_policy.setCurrentIndex(config['CMLC']['policy'])
        self.UI.comboBox_TSTM_policy.setCurrentIndex(config['TSTM']['policy'])
        self.UI.comboBox_AES_policy.setCurrentIndex(config['AES']['policy'])
        self.UI.groupBox_SLC_L2.setEnabled(False if(config['SLC']['level']==1) else True)
        self.UI.groupBox_CMLC_L2.setEnabled(False if(config['CMLC']['level']==1) else True)
        self.UI.groupBox_TSTM_L2.setEnabled(False if(config['TSTM']['level']==1) else True)
        self.UI.groupBox_AES_L2.setEnabled(False if(config['AES']['level']==1) else True)
        self.UI.comboBox_AES_threshold.setCurrentIndex(config['threshold'])
        
        # LineEdit setting:
        self.UI.lineEdit_SLC_datasize.setText(_translate("Dialog_Simulator", str(config['SLC']['datasize']), None))
        self.UI.lineEdit_SLC_L1_cachesize.setText(_translate("Dialog_Simulator", str(config['SLC']['L1'][0]), None))
        self.UI.lineEdit_SLC_L1_associativity.setText(_translate("Dialog_Simulator", str(config['SLC']['L1'][1]), None))
        self.UI.lineEdit_SLC_L2_cachesize.setText(_translate("Dialog_Simulator", str(config['SLC']['L2'][0]), None))
        self.UI.lineEdit_SLC_L2_associativity.setText(_translate("Dialog_Simulator", str(config['SLC']['L2'][1]), None))
        self.UI.lineEdit_CMLC_datasize.setText(_translate("Dialog_Simulator", str(config['CMLC']['datasize']), None))
        self.UI.lineEdit_CMLC_L1_cachesize.setText(_translate("Dialog_Simulator", str(config['CMLC']['L1'][0]), None))
        self.UI.lineEdit_CMLC_L1_associativity.setText(_translate("Dialog_Simulator", str(config['CMLC']['L1'][1]), None))
        self.UI.lineEdit_CMLC_L2_cachesize.setText(_translate("Dialog_Simulator", str(config['CMLC']['L2'][0]), None))
        self.UI.lineEdit_CMLC_L2_associativity.setText(_translate("Dialog_Simulator", str(config['CMLC']['L2'][1]), None))
        self.UI.lineEdit_TSTM_datasize.setText(_translate("Dialog_Simulator", str(config['TSTM']['datasize']), None))
        self.UI.lineEdit_TSTM_L1_cachesize.setText(_translate("Dialog_Simulator", str(config['TSTM']['L1'][0]), None))
        self.UI.lineEdit_TSTM_L1_associativity.setText(_translate("Dialog_Simulator", str(config['TSTM']['L1'][1]), None))
        self.UI.lineEdit_TSTM_L2_cachesize.setText(_translate("Dialog_Simulator", str(config['TSTM']['L2'][0]), None))
        self.UI.lineEdit_TSTM_L2_associativity.setText(_translate("Dialog_Simulator", str(config['TSTM']['L2'][1]), None))
        self.UI.lineEdit_AES_datasize.setText(_translate("Dialog_Simulator", str(config['AES']['datasize']), None))
        self.UI.lineEdit_AES_L1_cachesize.setText(_translate("Dialog_Simulator", str(config['AES']['L1'][0]), None))
        self.UI.lineEdit_AES_L1_associativity.setText(_translate("Dialog_Simulator", str(config['AES']['L1'][1]), None))
        self.UI.lineEdit_AES_L2_cachesize.setText(_translate("Dialog_Simulator", str(config['AES']['L2'][0]), None))
        self.UI.lineEdit_AES_L2_associativity.setText(_translate("Dialog_Simulator", str(config['AES']['L2'][1]), None))
        for file in config['Trace']['file']:
            self.UI.textEdit_traces.append(file)
        return
    def get_now_config(self):
        config = read_config()
        config['SLC']['enable'] = self.UI.checkBox_SLC.isChecked()
        config['SLC']['level'] = self.UI.comboBox_SLC.currentIndex()+1
        config['SLC']['datasize'] = int(self.UI.lineEdit_SLC_datasize.text())
        config['SLC']['policy'] = self.UI.comboBox_SLC_policy.currentIndex()
        config['SLC']['L1'] = [int(self.UI.lineEdit_SLC_L1_cachesize.text()),int(self.UI.lineEdit_SLC_L1_associativity.text())]
        config['SLC']['L2'] = [int(self.UI.lineEdit_SLC_L2_cachesize.text()),int(self.UI.lineEdit_SLC_L2_associativity.text())]
        config['CMLC']['enable'] = self.UI.checkBox_CMLC.isChecked()
        config['CMLC']['level'] = self.UI.comboBox_CMLC.currentIndex()+1
        config['CMLC']['datasize'] = int(self.UI.lineEdit_CMLC_datasize.text())
        config['CMLC']['policy'] = self.UI.comboBox_CMLC_policy.currentIndex()
        config['CMLC']['L1'] = [int(self.UI.lineEdit_CMLC_L1_cachesize.text()),int(self.UI.lineEdit_CMLC_L1_associativity.text())]
        config['CMLC']['L2'] = [int(self.UI.lineEdit_CMLC_L2_cachesize.text()),int(self.UI.lineEdit_CMLC_L2_associativity.text())]
        config['TSTM']['enable'] = self.UI.checkBox_TSTM.isChecked()
        config['TSTM']['level'] = self.UI.comboBox_TSTM.currentIndex()+1
        config['TSTM']['datasize'] = int(self.UI.lineEdit_TSTM_datasize.text())
        config['TSTM']['policy'] = self.UI.comboBox_TSTM_policy.currentIndex()
        config['TSTM']['L1'] = [int(self.UI.lineEdit_TSTM_L1_cachesize.text()),int(self.UI.lineEdit_TSTM_L1_associativity.text())]
        config['TSTM']['L2'] = [int(self.UI.lineEdit_TSTM_L2_cachesize.text()),int(self.UI.lineEdit_TSTM_L2_associativity.text())]
        config['AES']['enable'] = self.UI.checkBox_AES.isChecked()
        config['AES']['level'] = self.UI.comboBox_AES.currentIndex()+1
        config['AES']['datasize'] = int(self.UI.lineEdit_AES_datasize.text())
        config['AES']['policy'] = self.UI.comboBox_AES_policy.currentIndex()
        config['AES']['L1'] = [int(self.UI.lineEdit_AES_L1_cachesize.text()),int(self.UI.lineEdit_AES_L1_associativity.text())]
        config['AES']['L2'] = [int(self.UI.lineEdit_AES_L2_cachesize.text()),int(self.UI.lineEdit_AES_L2_associativity.text())]
        config['threshold'] = self.UI.comboBox_AES_threshold.currentIndex()
        config['Trace']['file'] = str(self.UI.textEdit_traces.toPlainText()).split('\n')
        return config
    def change_checkBox(self,scheme,state):
        print 'change_checkBox:',scheme,state
        if(scheme=='SLC'):
            self.UI.groupBox_SLC.setEnabled(bool(state))
        elif(scheme=='CMLC'):
            self.UI.groupBox_CMLC.setEnabled(bool(state))
        elif(scheme=='TSTM'):
            self.UI.groupBox_TSTM.setEnabled(bool(state))
        elif(scheme=='AES'):
            self.UI.groupBox_AES.setEnabled(bool(state))
        return
    def change_comboBox(self,scheme,index):
        print 'change_comboBox:',scheme , index
        if(scheme=='SLC'):
            self.UI.groupBox_SLC_L2.setEnabled(False if(index==0) else True)
            self.UI.groupBox_SLC_L1.setTitle(_translate("Dialog_Simulator", "L1 Cache(STT-RAM)" if(index==0) else "L1 Cache(SRAM)", None))
        elif(scheme=='CMLC'):
            self.UI.groupBox_CMLC_L2.setEnabled(False if(index==0) else True)
            self.UI.groupBox_CMLC_L1.setTitle(_translate("Dialog_Simulator", "L1 Cache(STT-RAM)" if(index==0) else "L1 Cache(SRAM)", None))
        elif(scheme=='TSTM'):
            self.UI.groupBox_TSTM_L2.setEnabled(False if(index==0) else True)
            self.UI.groupBox_TSTM_L1.setTitle(_translate("Dialog_Simulator", "L1 Cache(STT-RAM)" if(index==0) else "L1 Cache(SRAM)", None))
        elif(scheme=='AES'):
            self.UI.groupBox_AES_L2.setEnabled(False if(index==0) else True)
            self.UI.groupBox_AES_L1.setTitle(_translate("Dialog_Simulator", "L1 Cache(STT-RAM)" if(index==0) else "L1 Cache(SRAM)", None))
        return
    def click_pushButton(self,btn):
        global threads
        print 'click_pushButton:',btn
        schemes = ['SLC','CMLC','TSTM','AES']
        policy = ['LRU','FIFO']
        if(btn=='Options'):
            options.show()
        elif(btn=='Start'):
            config = self.get_now_config()
            save_config(config)
            #ChangeGlobalVar(config)
            if(not logprinter.isActiveWindow()):
                logprinter.show()
            if(not running):
                self.cacheManagers = list()
            """
            for manager in self.cacheManagers:
                manager.setParameters(bits_of_address=config['Trace']['address_bits'],bytes_of_trace_data=config['Trace']['data_bytes'])
            """
            for filename in config['Trace']['file']:
                if(filename==''):
                    continue
                fname = config['Trace']['path']+os.sep+filename
                for scheme in schemes:
                    if(not config[scheme]['enable']):
                        continue
                    _cache = {'SLC':pycache.SLCcache,'CMLC':pycache.MLCcache,'TSTM':TSTM.TSTMcache,'AES':AES.AEScache}[scheme]
                    manager = MyCacheManager()
                    manager.setParameters(bits_of_address=config['Trace']['address_bits'],bytes_of_trace_data=config['Trace']['data_bytes'])
                    if(config[scheme]['level']==1):
                        manager.OneLevelInit(_cache,
                            config[scheme]['L1'][0], config[scheme]['L1'][1],
                            data_bytes=config[scheme]['datasize'],
                            eviction_mode=policy[config[scheme]['policy']])
                    elif(config[scheme]['level']==2):
                        manager.TwoLevelInit(_cache,
                            config[scheme]['L1'][0], config[scheme]['L1'][1],
                            config[scheme]['L2'][0], config[scheme]['L2'][1],
                            data_bytes=config[scheme]['datasize'],
                            eviction_mode=policy[config[scheme]['policy']])
                    self.cacheManagers.append(manager)
                    threads.append([threading.Thread(target=self.cacheManagers[-1].Trace, args=(fname,)),fname,scheme,config])
            if(not running):
                threading.Thread(target=thr_manager).start()
            else:
                logprinter.log('\n\nSuccess to add new process.\n\n', ltype='hint')
        return
    def select_trace(self):
        files = list(QtGui.QFileDialog.getOpenFileNames(self,"Trace",read_config()['Trace']['path'],"Text Files (*.txt)"))
        if(not len(files)):
            return
        self.UI.textEdit_traces.clear()
        config = read_config()
        config['Trace']['file'] = []
        for file in files:
            fname = unicode(file.split('\\')[-1])
            config['Trace']['path'] = unicode(file.split('\\')[:-1].join('\\'))
            config['Trace']['file'].append(fname)
            self.UI.textEdit_traces.append(fname)
        save_config(config)
        
if __name__ == '__main__':  
    app = QtGui.QApplication(sys.argv)
    logprinter = Log_Printer()
    options = Options_Manager()
    simulator = STT_RAM_Simulator()
    simulator.show()
    for i in range(len(threads)):
        threads[i].join()
    app.exec_()
    """
    for r in os.walk('./RESULT/tmp'):
        if(len(r[1])):
            continue
        for filename in r[2]:
            os.remove('./RESULT/tmp/'+filename)
    """
    sys.exit()
